const ctx = document.getElementById("myChart");
var chartDataString = document.getElementById('chart_data').textContent;
var chartData = JSON.parse(chartDataString);

const data = {
            labels: chartData.applies_from_datetime,
            datasets: [{
                label: 'Actual',
                data: chartData.actual,
                borderColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 1
            },
            {
                label: 'Forecast',
                data: chartData.forecast,
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1,
                borderDash: [5, 5],
                spanGaps: true
            },
            {
                label: 'Historical Latest Forecast',
                data: chartData.historical_forecast,
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1,
                borderDash: [5, 5],
                spanGaps: true,
                hidden: true
            }]
        };

const options = {
    maintainAspectRatio: false,
            scales: {
                x: {
                    type: 'time', // Use time scale for x-axis
                    time: {
                        parser: 'yyyy-MM-dd HH:mm:ss', // Date format
                        unit: 'hour', // Display unit
                        displayFormats: {
                            hour: 'HH:mm' // Format for display
                        }
                    },
                    ticks: {
                        source: 'applies_from_datetime' // Use labels as tick data source
                    }
                },
                y: {
                    beginAtZero: true
                }
            }
        };

new Chart(ctx, {
    type: "line",
    data,
    options: options
});
