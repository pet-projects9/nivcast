import numpy as np
from flask_cors import CORS

from db.orm import VWLatestForecastData, get_query_as_df
from db.db_connection import dbm
from flask import Flask, render_template


app = Flask(__name__)
CORS(app)


@app.route('/')
def index():
    with dbm.get_managed_session() as session:
        query = session.query(VWLatestForecastData)
        forecast_df = get_query_as_df(query)
        forecast_df['applies_from_datetime'] = forecast_df['applies_from_datetime'].\
            dt.strftime('%Y-%m-%d %H:%M:%S')
        forecast_df['forecast_published'] = forecast_df['forecast_published'].\
            dt.strftime('%Y-%m-%d %H:%M:%S')
        forecast_df['actual_published'] = forecast_df['actual_published'].\
            dt.strftime('%Y-%m-%d %H:%M:%S')

        forecast_df['latest_forecast'] = forecast_df['forecast']
        forecast_df['historical_forecast'] = np.where(~forecast_df['actual'].isna(),
                                                      forecast_df['forecast'],
                                                      np.NaN)
        forecast_df['forecast'] = np.where(forecast_df['actual'].isna(),
                                           forecast_df['forecast'],
                                           np.NaN)

        # Join up forecast and actual
        last_available_actual_index = forecast_df[~forecast_df['actual'].isna()].index[0]
        forecast_df.loc[last_available_actual_index, 'forecast'] = forecast_df.loc[last_available_actual_index, 'actual']

        # Replace last forecast
        forecast_df = forecast_df.replace({np.NaN: '-'})
        forecast_df = forecast_df[['applies_from_datetime',
                                   'actual',
                                   'latest_forecast',
                                   'historical_forecast',
                                   'forecast',
                                   'forecast_published',
                                   'actual_published']]
        forecast_data = forecast_df.to_dict('list')

    return render_template('index.html', chart_data=forecast_data)


@app.route('/accuracy/')
def accuracy():
    return render_template('accuracy.html')


@app.route('/about/')
def about():
    return render_template('about.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0')
