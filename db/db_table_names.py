schema = 'public'


def bm_unit():
    table_name = 'bm_unit'
    return table_name


def grid_supply_point():
    table_name = 'grid_supply_point'
    return table_name


def fpn_series():
    table_name = 'fpn_series'
    return table_name


def curtailment_series():
    table_name = 'curtailment_series'
    return table_name


def fpn_series_item():
    table_name = 'fpn_series_item'
    return table_name


def curtailment_series_item():
    table_name = 'curtailment_series_item'
    return table_name


def weather_series():
    table_name = 'weather_series'
    return table_name


def weather_series_item():
    table_name = 'weather_series_item'
    return table_name


def system_series():
    table_name = 'system_series'
    return table_name


def system_series_item():
    table_name = 'system_series_item'
    return table_name


def supply_series():
    table_name = 'supply_series'
    return table_name


def supply_series_item():
    table_name = 'supply_series_item'
    return table_name



def demand_series():
    table_name = 'demand_series'
    return table_name


def demand_series_item():
    table_name = 'demand_series_item'
    return table_name


def model():
    table_name = 'model'
    return table_name


def job_logs():
    table_name = 'job_logs'
    return table_name


def daily_unit_curtailment():
    table_name = 'daily_unit_curtailment'
    return table_name


def sp_total_curtailment():
    table_name = 'sp_total_curtailment'
    return table_name


def bm_unit_grid_supply_point():
    table_name = 'bm_unit_grid_supply_point'
    return table_name


def grip_supply_point_group_avg_weather_actual():
    table_name = 'grip_supply_point_group_avg_weather_actual'
    return table_name


def grip_supply_point_group_avg_weather_forecast():
    table_name = 'grip_supply_point_group_avg_weather_forecast'
    return table_name


def avg_weather_forecast():
    table_name = 'avg_weather_forecast'
    return table_name


def avg_weather_actual():
    table_name = 'avg_weather_actual'
    return table_name


def vw_latest_forecast_data():
    table_name = 'vw_latest_forecast_data'
    return table_name
