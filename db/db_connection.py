import json
from contextlib import contextmanager
from os import environ, path

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
from sqlalchemy.exc import PendingRollbackError


ENV = 'production'
db_credentials_path = path.join(path.expanduser('~'), 'credentials', 'db_credentials.json')


class DatabaseManager:
    def __init__(self, db_credentials):
        self.db_credentials = db_credentials
        self.url = URL.create(**self.db_credentials)
        self.engine = create_engine(self.url).connect()

    @contextmanager
    def get_managed_session(self):
        Session = sessionmaker(bind=self.engine)
        session = Session()

        try:
            yield session
        except PendingRollbackError:
            # PendingRollbackErrors seem to be related to a closed connection
            # and can't be recovered from, so need to make a new connection
            self.engine = create_engine(self.url).connect()
        except:
            session.rollback()
            raise
        else:
            session.commit()
        finally:
            session.close()


if ENV == 'production':
    db_credentials_ = {"username": environ['DB_USERNAME'],
                       "password": environ['DB_PASSWORD'],
                       "host": environ['DATABASE_URL'],
                       "database": environ['DATABASE_NAME'],
                       "drivername": "postgresql",
                       "port": 25060}

elif ENV == 'development':
    with open(db_credentials_path) as json_file:
        db_credentials_ = json.load(json_file)
else:
    raise ValueError(f"ENV should be production or development, got {ENV}")

dbm = DatabaseManager(db_credentials_)
