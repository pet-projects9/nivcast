from datetime import datetime

import pandas as pd
from sqlalchemy import Column, Integer, String, Numeric, DateTime, ForeignKey
from geoalchemy2 import Geometry
from sqlalchemy.orm import relationship, declarative_base

import db.db_table_names as tb
from db.helper_functions import insert_bulk_items_from_df

Base = declarative_base()


class BMUnit(Base):
    __tablename__ = tb.bm_unit()

    unit_id = Column(Integer, primary_key=True)
    elexon_id = Column(String)
    centre_coords = Column(Geometry(geometry_type='POINT'))
    unit_name = Column(String)

    fpn_series = relationship('FPNSeries', back_populates='bm_unit')
    curtailment_series = relationship('CurtailmentSeries', back_populates='bm_unit')

    @classmethod
    def get_by_alternate_key(cls, session, elexon_id):
        return session.query(cls).filter_by(elexon_id=elexon_id).one()

    @classmethod
    def get_by_name(cls, session, unit_name):
        return session.query(cls).filter_by(unit_name=unit_name).all()

    @classmethod
    def create_or_update(cls, session, elexon_id, centre_coords, unit_name):
        try:
            unit = cls.get_by_alternate_key(session, elexon_id)
            return unit, True
        except:
            session.add(cls(elexon_id=elexon_id, centre_coords=centre_coords, unit_name=unit_name))
            session.commit()
            unit = cls.get_by_alternate_key(session, elexon_id=elexon_id)
            return unit, False


class GridSupplyPoint(Base):
    __tablename__ = tb.grid_supply_point()

    grid_point_uid = Column(Integer, primary_key=True)
    grid_supply_point_name = Column(String)
    grid_supply_point_group = Column(String)
    grid_geom = Column(Geometry(srid=4326))

    @classmethod
    def get_by_alternate_key(cls, session, grid_supply_point_name, grid_supply_point_group):
        return session.query(cls).\
            filter_by(grid_supply_point_name=grid_supply_point_name,
                      grid_supply_point_group=grid_supply_point_group).one()

    @classmethod
    def create_or_update(cls, session, grid_supply_point_name, grid_supply_point_group, grid_geom):
        try:
            gsp = cls.get_by_alternate_key(session, grid_supply_point_name, grid_supply_point_group)
            return gsp, True
        except:
            session.add(cls(grid_supply_point_name=grid_supply_point_name,
                            grid_supply_point_group=grid_supply_point_group,
                            grid_geom=grid_geom))
            session.commit()
            gsp = cls.get_by_alternate_key(session,
                                           grid_supply_point_name=grid_supply_point_name,
                                           grid_supply_point_group=grid_supply_point_group)
            return gsp, False


class Model(Base):
    __tablename__ = tb.model()

    model_uid = Column(Integer, primary_key=True)
    model_name = Column(String)

    weather_series = relationship('WeatherSeries', back_populates='model')
    curtailment_series = relationship('CurtailmentSeries', back_populates='model')
    fpn_series = relationship('FPNSeries', back_populates='model')
    demand_series = relationship('DemandSeries', back_populates='model')
    system_series = relationship('SystemSeries', back_populates='model')
    supply_series = relationship('SupplySeries', back_populates='model')

    @classmethod
    def get_by_name(cls, session, model_name):
        return session.query(cls).filter_by(model_name=model_name).one()

    @classmethod
    def create_or_update(cls, session, model_name):
        try:
            model = cls.get_by_name(session, model_name=model_name)
            return model, True
        except:
            session.add(cls(model_name=model_name))
            session.commit()
            model = cls.get_by_name(session, model_name=model_name)
            return model, False


class FPNSeries(Base):
    __tablename__ = tb.fpn_series()
    series_uid = Column(Integer, primary_key=True)
    unit_id = Column(Integer, ForeignKey(BMUnit.unit_id))
    frequency = Column(String)
    unit = Column(String)
    projection_type = Column(String)
    model_uid = Column(Integer, ForeignKey(Model.model_uid))

    bm_unit = relationship('BMUnit', back_populates='fpn_series')
    fpn_series_item = relationship('FPNSeriesItem', back_populates='fpn_series')
    model = relationship('Model', back_populates='fpn_series')

    @classmethod
    def get_by_alternate_key(cls, session, unit_id, frequency, unit, projection_type, model_uid):
        return session.query(cls).filter_by(unit_id=unit_id, frequency=frequency, unit=unit,
                                            projection_type=projection_type, model_uid=model_uid).one()

    @classmethod
    def create_or_update(cls, session, unit_id, frequency, unit, projection_type, model_uid):
        try:
            fpn_series = cls.get_by_alternate_key(session, unit_id=unit_id, frequency=frequency, unit=unit,
                                                  projection_type=projection_type, model_uid=model_uid)
            return fpn_series, True
        except:
            session.add(cls(unit_id=unit_id, frequency=frequency, unit=unit, projection_type=projection_type,
                            model_uid=model_uid))
            session.commit()
            fpn_series = cls.get_by_alternate_key(session, unit_id=unit_id, frequency=frequency, unit=unit,
                                                  projection_type=projection_type, model_uid=model_uid)
            return fpn_series, False

    def insert_items_from_df(self, session, df):
        required_columns = ['applies_from_datetime', 'latest_info_datetime', 'quantity']
        # Add series uid to dataframe
        df['series_uid'] = self.series_uid
        missing_columns = [col for col in required_columns if col not in df.columns]
        if missing_columns:
            raise ValueError(f"Missing columns {missing_columns}")
        insert_bulk_items_from_df(conn=session.bind.connection, df=df, table=tb.fpn_series_item())


class FPNSeriesItem(Base):
    __tablename__ = tb.fpn_series_item()

    series_item_uid = Column(Integer, primary_key=True)
    series_uid = Column(Integer, ForeignKey(FPNSeries.series_uid))
    applies_from_datetime = Column(DateTime)
    latest_info_datetime = Column(DateTime)
    quantity = Column(Numeric)

    fpn_series = relationship('FPNSeries', back_populates='fpn_series_item')

    @classmethod
    def get_by_uid(cls, session, uid):
        return session.query(cls).get(uid)


class DemandSeries(Base):
    __tablename__ = tb.demand_series()
    series_uid = Column(Integer, primary_key=True)
    location_name = Column(String)
    category = Column(String)
    frequency = Column(String)
    projection_type = Column(String)
    data_source = Column(String)
    model_uid = Column(Integer, ForeignKey(Model.model_uid))

    demand_series_item = relationship('DemandSeriesItem', back_populates='demand_series')
    model = relationship('Model', back_populates='demand_series')

    @classmethod
    def get_by_alternate_key(cls, session, location_name, category, frequency, projection_type,
                             data_source, model_uid=None):
        return session.query(cls).filter_by(location_name=location_name, category=category,
                                            frequency=frequency, projection_type=projection_type,
                                            data_source=data_source, model_uid=model_uid).one()

    @classmethod
    def create_or_update(cls, session, location_name, category, frequency, projection_type,
                         data_source, model_uid=None):
        try:
            demand_series = cls.get_by_alternate_key(session, location_name=location_name, category=category,
                                                     frequency=frequency, projection_type=projection_type,
                                                     data_source=data_source, model_uid=model_uid)
            return demand_series, True
        except:
            session.add(cls(location_name=location_name, category=category,
                            frequency=frequency, projection_type=projection_type,
                            data_source=data_source, model_uid=model_uid))
            session.commit()
            demand_series = cls.get_by_alternate_key(session, location_name=location_name, category=category,
                                                     frequency=frequency, projection_type=projection_type,
                                                     data_source=data_source, model_uid=model_uid)
            return demand_series, False

    def insert_items_from_df(self, session, df):
        required_columns = ['applies_from_datetime', 'settlement_period', 'latest_info_datetime', 'quantity']
        # Add series uid to dataframe
        df['series_uid'] = self.series_uid
        missing_columns = [col for col in required_columns if col not in df.columns]
        if missing_columns:
            raise ValueError(f"Missing columns {missing_columns}")
        insert_bulk_items_from_df(conn=session.bind.connection, df=df, table=tb.demand_series_item())


class DemandSeriesItem(Base):
    __tablename__ = tb.demand_series_item()

    series_item_uid = Column(Integer, primary_key=True)
    series_uid = Column(Integer, ForeignKey(DemandSeries.series_uid))
    applies_from_datetime = Column(DateTime)
    settlement_period = Column(Integer)
    latest_info_datetime = Column(DateTime)
    quantity = Column(Numeric)

    demand_series = relationship('DemandSeries', back_populates='demand_series_item')

    @classmethod
    def get_by_uid(cls, session, uid):
        return session.query(cls).get(uid)


class CurtailmentSeries(Base):
    __tablename__ = tb.curtailment_series()
    series_uid = Column(Integer, primary_key=True)
    unit_id = Column(Integer, ForeignKey(BMUnit.unit_id))
    frequency = Column(String)
    unit = Column(String)
    model_uid = Column(Integer, ForeignKey(Model.model_uid))

    bm_unit = relationship('BMUnit', back_populates='curtailment_series')
    curtailment_series_item = relationship('CurtailmentSeriesItem', back_populates='curtailment_series')
    model = relationship('Model', back_populates='curtailment_series')

    @classmethod
    def get_by_alternate_key(cls, session, unit_id, frequency, unit):
        return session.query(cls).filter_by(unit_id=unit_id, frequency=frequency, unit=unit).one()

    @classmethod
    def create_or_update(cls, session, unit_id, frequency, unit):
        try:
            fpn_series = cls.get_by_alternate_key(session, unit_id=unit_id, frequency=frequency, unit=unit)
            return fpn_series, True
        except:
            session.add(cls(unit_id=unit_id, frequency=frequency, unit=unit))
            session.commit()
            fpn_series = cls.get_by_alternate_key(session, unit_id=unit_id, frequency=frequency, unit=unit)
            return fpn_series, False

    def insert_items_from_df(self, session, df):
        required_columns = ['applies_from_datetime', 'quantity']
        # Add series uid to dataframe
        df['series_uid'] = self.series_uid
        missing_columns = [col for col in required_columns if col not in df.columns]
        if missing_columns:
            raise ValueError(f"Missing columns {missing_columns}")
        insert_bulk_items_from_df(conn=session.bind.connection, df=df, table=tb.curtailment_series_item())


class CurtailmentSeriesItem(Base):
    __tablename__ = tb.curtailment_series_item()

    series_item_uid = Column(Integer, primary_key=True)
    series_uid = Column(Integer, ForeignKey(CurtailmentSeries.series_uid))
    applies_from_datetime = Column(DateTime)
    latest_info_datetime = Column(DateTime)
    quantity = Column(Numeric)

    curtailment_series = relationship('CurtailmentSeries', back_populates='curtailment_series_item')

    @classmethod
    def get_by_uid(cls, session, uid):
        return session.query(cls).get(uid)


class SystemSeries(Base):
    __tablename__ = tb.system_series()
    series_uid = Column(Integer, primary_key=True)
    series_name = Column(String)
    frequency = Column(String)
    source = Column(String)
    projection_type = Column(String)
    unit = Column(String)
    model_uid = Column(Integer, ForeignKey(Model.model_uid))

    model = relationship('Model', back_populates='system_series')
    system_series_item = relationship('SystemSeriesItem', back_populates='system_series')

    @classmethod
    def get_by_alternate_key(cls, session, series_name, frequency, source, projection_type, unit,
                             model_uid=None):
        return session.query(cls).filter_by(series_name=series_name, frequency=frequency, source=source,
                                            projection_type=projection_type, unit=unit,
                                            model_uid=model_uid).one()

    @classmethod
    def create_or_update(cls, session, series_name, frequency, source, projection_type, unit,
                         model_uid=None):
        try:
            system_series = cls.get_by_alternate_key(session, series_name=series_name, frequency=frequency,
                                                     source=source, projection_type=projection_type, unit=unit,
                                                     model_uid=model_uid)
            return system_series, True
        except:
            session.add(cls(series_name=series_name, frequency=frequency, source=source,
                            projection_type=projection_type, unit=unit, model_uid=model_uid))
            session.commit()
            system_series = cls.get_by_alternate_key(session, series_name=series_name, frequency=frequency,
                                                     source=source, projection_type=projection_type, unit=unit,
                                                     model_uid=model_uid)
            return system_series, False

    def insert_items_from_df(self, session, df):
        required_columns = ['applies_from_datetime', 'quantity']
        # Add series uid to dataframe
        df['series_uid'] = self.series_uid
        missing_columns = [col for col in required_columns if col not in df.columns]
        if missing_columns:
            raise ValueError(f"Missing columns {missing_columns}")
        insert_bulk_items_from_df(conn=session.bind.connection, df=df, table=tb.system_series_item())


class SystemSeriesItem(Base):
    __tablename__ = tb.system_series_item()

    series_item_uid = Column(Integer, primary_key=True)
    series_uid = Column(Integer, ForeignKey(SystemSeries.series_uid))
    applies_from_datetime = Column(DateTime)
    latest_info_datetime = Column(DateTime)
    quantity = Column(Numeric)

    system_series = relationship('SystemSeries', back_populates='system_series_item')

    @classmethod
    def get_by_uid(cls, session, uid):
        return session.query(cls).get(uid)


class SupplySeries(Base):
    __tablename__ = tb.supply_series()
    series_uid = Column(Integer, primary_key=True)
    series_name = Column(String)
    frequency = Column(String)
    source = Column(String)
    projection_type = Column(String)
    unit = Column(String)
    model_uid = Column(Integer, ForeignKey(Model.model_uid))

    model = relationship('Model', back_populates='supply_series')
    supply_series_item = relationship('SupplySeriesItem', back_populates='supply_series')

    @classmethod
    def get_by_alternate_key(cls, session, series_name, frequency, source, projection_type, unit,
                             model_uid=None):
        return session.query(cls).filter_by(series_name=series_name, frequency=frequency, source=source,
                                            projection_type=projection_type, unit=unit,
                                            model_uid=model_uid).one()

    @classmethod
    def create_or_update(cls, session, series_name, frequency, source, projection_type, unit,
                         model_uid=None):
        try:
            supply_series = cls.get_by_alternate_key(session, series_name=series_name, frequency=frequency,
                                                     source=source, projection_type=projection_type, unit=unit,
                                                     model_uid=model_uid)
            return supply_series, True
        except:
            session.add(cls(series_name=series_name, frequency=frequency, source=source,
                            projection_type=projection_type, unit=unit, model_uid=model_uid))
            session.commit()
            supply_series = cls.get_by_alternate_key(session, series_name=series_name, frequency=frequency,
                                                     source=source, projection_type=projection_type, unit=unit,
                                                     model_uid=model_uid)
            return supply_series, False

    def insert_items_from_df(self, session, df):
        required_columns = ['applies_from_datetime', 'quantity']
        # Add series uid to dataframe
        df['series_uid'] = self.series_uid
        missing_columns = [col for col in required_columns if col not in df.columns]
        if missing_columns:
            raise ValueError(f"Missing columns {missing_columns}")
        insert_bulk_items_from_df(conn=session.bind.connection, df=df, table=tb.supply_series_item())


class SupplySeriesItem(Base):
    __tablename__ = tb.supply_series_item()

    series_item_uid = Column(Integer, primary_key=True)
    series_uid = Column(Integer, ForeignKey(SupplySeries.series_uid))
    applies_from_datetime = Column(DateTime)
    latest_info_datetime = Column(DateTime)
    quantity = Column(Numeric)

    supply_series = relationship('SupplySeries', back_populates='supply_series_item')

    @classmethod
    def get_by_uid(cls, session, uid):
        return session.query(cls).get(uid)


class WeatherSeries(Base):
    __tablename__ = tb.weather_series()
    series_uid = Column(Integer, primary_key=True)
    unit_name = Column(String)
    frequency = Column(String)
    projection_type = Column(String)
    model_uid = Column(Integer, ForeignKey(Model.model_uid))

    weather_series_item = relationship('WeatherSeriesItem', back_populates='weather_series')
    model = relationship('Model', back_populates='weather_series')

    @classmethod
    def get_by_alternate_key(cls, session, unit_name, frequency, projection_type, model_uid=None):
        return session.query(cls).filter_by(unit_name=unit_name, frequency=frequency,
                                            projection_type=projection_type, model_uid=model_uid).one()

    @classmethod
    def create_or_update(cls, session, unit_name, frequency, projection_type, model_uid=None):
        try:
            weather_series = cls.get_by_alternate_key(session, unit_name=unit_name, frequency=frequency,
                                                      projection_type=projection_type, model_uid=model_uid)
            return weather_series, True
        except:
            session.add(cls(unit_name=unit_name, frequency=frequency, projection_type=projection_type,
                            model_uid=model_uid))
            session.commit()
            weather_series = cls.get_by_alternate_key(session, unit_name, frequency=frequency,
                                                      projection_type=projection_type, model_uid=model_uid)
            return weather_series, False

    def insert_items_from_df(self, session, df):
        required_columns = ['applies_from_datetime', 'latest_info_datetime', 'wind_speed',
                            'wind_direction', 'wind_gust', 'temperature', 'pressure']
        # Add series uid to dataframe
        df['series_uid'] = self.series_uid
        missing_columns = [col for col in required_columns if col not in df.columns]
        if missing_columns:
            raise ValueError(f"Missing columns {missing_columns}")
        insert_bulk_items_from_df(conn=session.bind.connection, df=df, table=tb.weather_series_item())


class WeatherSeriesItem(Base):
    __tablename__ = tb.weather_series_item()

    series_item_uid = Column(Integer, primary_key=True)
    series_uid = Column(Integer, ForeignKey(WeatherSeries.series_uid))
    applies_from_datetime = Column(DateTime)
    latest_info_datetime = Column(DateTime)
    temperature = Column(Numeric)
    wind_gust = Column(Numeric)
    wind_speed = Column(Numeric)
    wind_direction = Column(Numeric)
    pressure = Column(Numeric)

    weather_series = relationship('WeatherSeries', back_populates='weather_series_item')

    @classmethod
    def get_by_uid(cls, session, uid):
        return session.query(cls).get(uid)


class JobLogs(Base):
    __tablename__ = tb.job_logs()

    job_run_uid = Column(Integer, primary_key=True)
    job_name = Column(String)
    job_result = Column(String)
    msg = Column(String)
    creation_datetime = Column(DateTime)

    @classmethod
    def insert_job_run_entry(cls, session, job_name, job_result, msg):
        session.add(cls(job_name=job_name, job_result=job_result, msg=msg,
                        creation_datetime=datetime.utcnow()))
        session.commit()


# Materialized views
class DailyUnitCurtailment(Base):
    __tablename__ = tb.daily_unit_curtailment()

    applies_from_datetime = Column(DateTime, primary_key=True)
    quantity = Column(Numeric)
    elexon_id = Column(String, primary_key=True)
    unit_name = Column(String)
    unit_id = Column(Integer)


class SPTotalCurtailment(Base):
    __tablename__ = tb.sp_total_curtailment()

    applies_from_datetime = Column(DateTime, primary_key=True)
    quantity = Column(Numeric)


class BMUnitGridSupplyPoint(Base):
    __tablename__ = tb.bm_unit_grid_supply_point()

    bm_unit_id = Column(Integer, primary_key=True)
    unit_name = Column(String)
    grid_supply_point_name = Column(String)
    grid_supply_point_group = Column(String)


class GridSupplyPointGroupAvgWeatherForecast(Base):
    __tablename__ = tb.grip_supply_point_group_avg_weather_forecast()

    applies_from_datetime = Column(DateTime, primary_key=True)
    grid_supply_point_group = Column(DateTime, primary_key=True)
    avg_gsp_wind_speed = Column(Numeric)
    avg_gsp_wind_gust = Column(Numeric)


class GridSupplyPointGroupAvgWeatherActual(Base):
    __tablename__ = tb.grip_supply_point_group_avg_weather_actual()

    applies_from_datetime = Column(DateTime, primary_key=True)
    grid_supply_point_group = Column(DateTime, primary_key=True)
    avg_gsp_wind_speed = Column(Numeric)
    avg_gsp_wind_gust = Column(Numeric)


class AvgWeatherActual(Base):
    __tablename__ = tb.avg_weather_actual()

    applies_from_datetime = Column(DateTime, primary_key=True)
    avg_wind_speed = Column(Numeric)
    avg_wind_gust = Column(Numeric)


class AvgWeatherForecast(Base):
    __tablename__ = tb.avg_weather_forecast()

    applies_from_datetime = Column(DateTime, primary_key=True)
    avg_wind_speed = Column(Numeric)
    avg_wind_gust = Column(Numeric)


class VWLatestForecastData(Base):
    __tablename__ = tb.vw_latest_forecast_data()

    applies_from_datetime = Column(DateTime, primary_key=True)
    actual = Column(Numeric)
    forecast = Column(Numeric)
    forecast_published = Column(DateTime)
    actual_published = Column(DateTime)


def get_query_as_df(query):
    return pd.read_sql(query.statement, query.session.bind)
