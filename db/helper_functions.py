import numpy as np
import psycopg2
from psycopg2 import extras


def insert_bulk_items_from_df(conn, df, table):
    """
    Using psycopg2.extras.execute_values() to insert the dataframe
    """
    # Handle NaN
    df = df.replace({np.NaN: None, np.Inf: None})

    # Convert dataframe
    tuples = [tuple(x) for x in df.to_numpy()]
    cols = ','.join(list(df.columns))

    # Insert statement
    query = "INSERT INTO %s(%s) VALUES %%s ON CONFLICT DO NOTHING" % (table, cols)
    cursor = conn.cursor()
    try:
        extras.execute_values(cursor, query, tuples)
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
    cursor.close()
